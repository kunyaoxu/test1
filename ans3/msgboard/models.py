# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Post(models.Model):
	text = models.TextField()
	published_date = models.DateTimeField(auto_now_add=True)

	def publish(self):
		# self.published_date = timezone.now()
		self.save()
		return self.pk

	def __unicode__(self):
		return self.text
