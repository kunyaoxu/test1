# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Post


class apiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        # fields = '__all__' #這個寫法表示使用全部的fields
        fields = ('pk', 'text', 'published_date')