Issues
===============================================================================

Issue 1. Support RESTful api service
-------------------------------------------------------------------------------

Please use djangorestframework==3.6.X to build the create_message api.
And finish this feature in issue#1-hoamon-feature-support-restful-api-service branch.

The api requires:

1. Using POST method( Definitely. it is RESTful api )
2. Using ModelSerializers ( http://www.django-rest-framework.org/tutorial/1-serialization/#using-modelserializers )
3. Using generic class-based views ( http://www.django-rest-framework.org/tutorial/3-class-based-views/#using-generic-class-based-views )
4. Follow TDD: finish this issue with two types of TestCase,
    1. simple request testcase, ex: django.test.Client( https://docs.djangoproject.com/en/1.11/topics/testing/tools/#making-requests ) or anything else.
    2. use selenium to build the functional testcase in browser( https://www.obeythetestinggoat.com/book/chapter_01.html )